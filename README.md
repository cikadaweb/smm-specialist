# SMM specialist manager

#### This project is a web application for processing customer requests.

[Project link](https://smm-specialist-f2c6a.firebaseapp.com/)

### Technologies

- Vue 3, Vue Router, Pinia
- TypeScript
- Vuetify 3
- Firebase Auth, Database, Storage, Hosting

## Project setup

```
# yarn
yarn

# npm
npm install

# pnpm
pnpm install
```

### Compiles and hot-reloads for development

```
# yarn
yarn dev

# npm
npm run dev

# pnpm
pnpm dev
```

### Compiles and minifies for production

```
# yarn
yarn build

# npm
npm run build

# pnpm
pnpm build
```

### Customize configuration

See [Configuration Reference](https://vitejs.dev/config/).
