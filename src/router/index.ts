// Composables
import { createRouter, createWebHistory } from 'vue-router'
import { getAuth, onAuthStateChanged } from "firebase/auth";

const routes = [
  {
    path: '/',
    component: () => import('@/layouts/default/Default.vue'),
    children: [
      {
        path: '',
        name: 'HomePage',
        component: () => import('@/views/HomePage.vue'),
      },
    ],
  },
    {
    path: '/product/:id',
    component: () => import('@/layouts/default/Default.vue'),
    children: [
      {
        path: '',
        name: 'ProductPage',
        component: () => import('@/views/ProductPage.vue'),
      },
    ],
  },
  {
    path: '/list',
    component: () => import('@/layouts/default/Default.vue'),
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: '',
        name: 'ProductListPage',
        component: () =>
          import('@/views/ProductListPage.vue'),
      },
    ],
  },
  {
    path: '/new',
    component: () => import('@/layouts/default/Default.vue'),
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: '',
        name: 'ProductCreatePage',
        component: () =>
          import('@/views/ProductCreatePage.vue'),
      },
    ],
  },
  {
    path: '/checkout',
    component: () => import('@/layouts/default/Default.vue'),
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: '',
        name: 'CheckoutPage',
        component: () =>
          import('@/views/CheckoutPage.vue'),
      },
    ],
  },
  {
    path: '/login',
    component: () => import('@/layouts/default/Default.vue'),
    children: [
      {
        path: '',
        name: 'LoginPage',
        component: () =>
          import('@/views/LoginPage.vue'),
      },
    ],
  },
  {
    path: '/register',
    component: () => import('@/layouts/default/Default.vue'),
    children: [
      {
        path: '',
        name: 'RegisterPage',
        component: () =>
          import('@/views/RegisterPage.vue'),
      },
    ],
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

const getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const removeListener = onAuthStateChanged(
      getAuth(),
      (user) => {
        removeListener();
        resolve(user);
      },
      reject
    )
  })
}

router.beforeEach(async (to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (await getCurrentUser()) {
      next();
    } else {
      next('/login?loginError=true');
    }
  } else {
    next();
  }
});

export default router
