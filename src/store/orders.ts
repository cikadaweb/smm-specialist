import { defineStore } from 'pinia';

import { IOrder, IOrderWithId } from '@/store/orders.interfaces';

import { useCommonStore } from '@/store/common';
import { useUserStore } from '@/store/user';

import { getDatabase, ref, push, onValue, get, update } from 'firebase/database';
import { FirebaseError } from 'firebase/app';

export const useOrderStore = defineStore('orders', {
  state: () => ({
    orders: [] as IOrderWithId[]
  }),
  actions: {
    async createOrder(payload: IOrder) {
      const commonStore = useCommonStore();
      commonStore.clearError();
      try {
        const newOrder = {
          productId: payload.productId,
          personName: payload.personName,
          personPhone: payload.personPhone,
          isDone: false,
        } as IOrder;
        // console.log('newOrder: ', newOrder);

        const database = getDatabase();

        const ordersRef = ref(database, `/orders`);
        const order = await push(ordersRef, newOrder);
      } catch (error) {
        const firebaseError = error as FirebaseError;
        commonStore.setError(firebaseError.message);
        throw firebaseError
      }
    },
    async fetchOrders() {
      const commonStore = useCommonStore();
      const userStore = useUserStore();

      commonStore.clearError();
      commonStore.setLoading(true);
      try {
        const database = getDatabase();
        const ordersRef = ref(database, `/orders`);

        const snapshot = await get(ordersRef);
        const orders = snapshot.val();

        // получение заказов с их id
        const ordersData = Object.keys(orders).map((orderId) => {
          const order = orders[orderId];
          return {
            ...order,
            id: orderId,
          };
        });
        // console.log('ordersData: ', ordersData);
        this.orders = ordersData;

        commonStore.setLoading(false);
      } catch (error) {
        const firebaseError = error as FirebaseError;
        commonStore.setLoading(false);
        commonStore.setError(firebaseError.message);
        throw firebaseError
      }
    },
    async markOrderDone(order: IOrderWithId) {
      // console.log('order: ', order);
      const commonStore = useCommonStore();
      const userStore = useUserStore();

      commonStore.clearError();
      commonStore.setLoading(true);
      try {
        const database = getDatabase();

        // обновляем данные заказа
        const orderRef = ref(database, `/orders/${order.id}`);
        let newStatus = true;
        if (order.isDone) {
          newStatus = false;
        }
        await update(orderRef, {
          isDone: newStatus,
        });

        this.fetchOrders();
        commonStore.setLoading(false);
      } catch (error) {
        const firebaseError = error as FirebaseError;
        commonStore.setLoading(false);
        commonStore.setError(firebaseError.message);
        throw firebaseError
      }
    },
    doneOrders(): IOrder[] {
      return this.orders.filter(order => order.isDone);
    },
    undoneOrders(): IOrder[] {
      return this.orders.filter(order => !order.isDone);
    },
    getAllOrders(): IOrder[] {
      const undoneOrders = this.undoneOrders();
      const doneOrders = this.doneOrders();
      return [...undoneOrders, ...doneOrders];
    }
  },
})
