import { defineStore } from 'pinia';

import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut } from 'firebase/auth';
import { FirebaseError } from 'firebase/app';

import router from '@/router';

import { useCommonStore } from '@/store/common';

import { IUser, IUserAuth } from '@/store/user.interfaces';

export const useUserStore = defineStore('user', {
  state: () => ({
    user: {
      id: null,
    } as IUser
  }),
  actions: {
    async registerUser(payload: IUserAuth) {
      const auth = getAuth();
      const commonStore = useCommonStore();
      commonStore.clearError();
      commonStore.setLoading(true);
      try {
        const user = await createUserWithEmailAndPassword(auth, payload.email, payload.password)
        this.setUser(user.user.uid);
        commonStore.setLoading(false);
        router.push('/');
      } catch (error) {
        const firebaseError = error as FirebaseError;
        commonStore.setLoading(false);
        commonStore.setError(firebaseError.message);
        throw firebaseError
      }
    },
    async loginUser(payload: IUserAuth) {
      const auth = getAuth();
      const commonStore = useCommonStore();
      commonStore.clearError();
      commonStore.setLoading(true);
      try {
        const user = await signInWithEmailAndPassword(auth, payload.email, payload.password);
        this.setUser(user.user.uid);
        commonStore.setLoading(false);
        router.push('/');
      } catch (error) {
        const firebaseError = error as FirebaseError;
        commonStore.setLoading(false);
        let errorMessage = '';
        switch (firebaseError.code) {
          case 'auth/invalid-email':
            errorMessage = 'Неверный email!';
            break;
          case 'auth/user-not-found':
            errorMessage = 'Пользователь с таким email не найден!';
            break;
          case 'auth/wrong-password':
            errorMessage = 'Неверный пароль!';
            break
          default:
            errorMessage = 'Email или пароль неверны!';
            break;
        }
        commonStore.setError(errorMessage);
        throw firebaseError
      }
    },
    setUser(payload: string | null) {
      this.user.id = payload;
    },
    autoLoginUser(payload: string) {
      this.setUser(payload)
    },
    async logoutUser() {
      const auth = getAuth();
      const commonStore = useCommonStore();
      commonStore.clearError();
      commonStore.setLoading(true);
      try {
        await signOut(auth);
        this.setUser(null);
        commonStore.setLoading(false);
      } catch (error) {
        const firebaseError = error as FirebaseError;
        commonStore.setLoading(false);
        commonStore.setError(firebaseError.message);
        throw firebaseError
      }
    },
    getCurrnetUser(this) {
      return this.user.id;
    },
    isUserLoggedIn(this) {
      return this.user.id !== null;
    }
  },
})
