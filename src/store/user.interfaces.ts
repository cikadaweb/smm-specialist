export interface IUser {
  id: string | null,
}

export interface IUserAuth {
  email: string,
  password: string
}
