export interface IProduct {
  id: number,
  title: string,
  description: string,
  price: string,
  promo: boolean,
  imagePath: string,
}

export interface IUpdateProduct {
  id: number,
  title: string,
  description: string,
}

export interface IProductCreate {
  title: string;
  description: string;
  price: string;
  promo: boolean;
  imagePath: File;
}
