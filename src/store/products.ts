// Utilities
import { defineStore } from 'pinia'
import { IProduct, IUpdateProduct, IProductCreate } from '@/store/products.interfaces'

import { useCommonStore } from '@/store/common'

import { getDatabase, ref, push, get, update } from 'firebase/database'
import { getStorage, ref as storageRef, uploadBytes, getDownloadURL } from 'firebase/storage';
import { FirebaseError } from 'firebase/app';

import router from '@/router'

export const useProductStore = defineStore('products', {
  state: () => ({
    products: [] as IProduct[],
    currentProduct: {} as IProduct,
  }),
  actions: {
    async fetchOneProduct(productId: any) {
      const commonStore = useCommonStore();
      commonStore.clearError();
      commonStore.setLoading(true);
      try {
        const database = getDatabase();
        const productRef = ref(database, `products/${productId}`);

        const productSnapshot = await get(productRef);
        const productData = productSnapshot.val();

        // Добавление id продукта
        const productAllData = {...productData, id: productId }

        if (productAllData) {
          this.currentProduct = productAllData;
        }
        commonStore.setLoading(false);
      } catch (error) {
        const firebaseError = error as FirebaseError;
        commonStore.setLoading(false);
        commonStore.setError(firebaseError.message);
        throw firebaseError
      }
    },
    async fetchProducts() {
      const commonStore = useCommonStore();
      commonStore.clearError();
      commonStore.setLoading(true);
      try {
        const database = getDatabase();
        const productsRef = ref(database, 'products');

        const snapshot = await get(productsRef);
        const products = snapshot.val();

        // для получения массива с ключом
        const productsData = Object.keys(products).map((productId) => {
          const product = products[productId];
          return {
            ...product,
            id: productId,
          };
        });

        this.products = productsData;
        commonStore.setLoading(false);
      } catch (error) {
        const firebaseError = error as FirebaseError;
        commonStore.setLoading(false);
        commonStore.setError(firebaseError.message);
        throw firebaseError
      }
    },
    async createProduct(payload: IProductCreate) {
      const commonStore = useCommonStore();
      commonStore.clearError();
      commonStore.setLoading(true);

      const image = payload.imagePath;

      try {
        const newProduct = {
          title: payload.title,
          description: payload.description,
          price: payload.price,
          promo: payload.promo,
          imagePath: '',
        } as IProduct;

        const database = getDatabase();
        const storage = getStorage();

        const productsRef = ref(database, 'products');
        const product = await push(productsRef, newProduct);

        // Получаем расширение файла
        const imageExt = image.name.slice(image.name.lastIndexOf('.'));
        // Создаем ссылку на объект в Cloud Storage с именем продукта и расширением файла
        const storageRefPath  = storageRef(storage, `products/${product.key}.${imageExt}`);
        // Загрузка содержимого файла в эту ссылку
        await uploadBytes(storageRefPath, image);
        // Получение URL файла.
        const imagePath = await getDownloadURL(storageRefPath);

        // Сохраняем URL в базе данных
        await update(ref(database, `products/${product.key}`), { imagePath })

        // console.log('imageExt: ', imageExt);
        // console.log('fileData: ', fileData);
        // console.log('imageSrc: ', imagePath);

        this.fetchProducts();
        router.push('/list');
        commonStore.setLoading(false);
      } catch (error) {
        const firebaseError = error as FirebaseError;
        commonStore.setLoading(false);
        commonStore.setError(firebaseError.message);
        throw firebaseError
      }
    },
    async updateProduct(payload: IUpdateProduct) {
      const commonStore = useCommonStore();
      commonStore.clearError();
      commonStore.setLoading(true);
      try {
        const database = getDatabase();
        // обновляем данные продукта
        const productRef = ref(database, `products/${payload.id}`);
        await update(productRef, {
          title: payload.title,
          description: payload.description,
        });

        this.fetchProducts();
        router.push('/list');
        commonStore.setLoading(false);
      } catch (error) {
        const firebaseError = error as FirebaseError;
        commonStore.setLoading(false);
        commonStore.setError(firebaseError.message);
        throw firebaseError
      }
    },
  },
})
