export interface IOrder {
  productId: number,
  personName: string,
  personPhone: string,
  isDone: boolean
}

export type IOrderWithId = Omit<IOrder, 'id'> & { id: string };
